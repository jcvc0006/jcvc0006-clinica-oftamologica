# Clínica Oftalmológica


## Fecha: 30-1-2024

**Antecedentes:** Hace tiempo mi tío me comunicó que compró un servidor y solicitó los servicios de una empresa para la elaboración de una página web que mostrara información de su clínica de oftalmología.

Cuando vi dicha página, me di cuenta de que podría ofrecer más servicios que solo mostrar la información de la consulta.

**Objetivos Iniciales:** 
- Mejorar el diseño de la mejorando como consecuencia la usabilidad de la misma.
- Implementar una **base de datos que almacene como usuarios a los diferentes clientes y les muestre información particular**, indicando por ejemplo los factores de riesgo que deben evitar según sus enfermedades, información sobre su enfermedad, fechas de su consulta y diagnósticos de las mismas...
- Gracias a la base de datos anterior podremos almacenar datos de los clientes que nos servirán para **gestionar de manera eficiente el fechado de las consultas de los mismos en función del riesgo de sus diferentes enfermedades**.
- A la hora de solicitar cita previa, la pestaña que aparece da lugar a ambigüedades, **quizás el implementar un selector de opciones posibles ayudaría a ser más específico** y a prever de manera sencilla la gravedad de la situación.

## Fecha: 6-2-2024
Después de charlar con mi tío, hemos llegado a la conclusión de que mostrar el historial médico de un cliente aunque lo hagamos accediendo mediante usuario y contraseña podría poner en peligro su privacidad. **Por lo que descartamos el mostrar información de su historial médico particular.**

  

## Fecha: 13-2-2024

Después de haber estudiado el segundo bloque del temario y de haber comprendido los conceptos del mismo se me han ocurrido **nuevos objetivos que debe tener el sistema hipermedia:**

- **Sistema de recomendación de contenidos y de muestreo de información adaptativo:** este sistema adaptara la estructura del hiperdocumeto, en función de la navegación del usuario. (Esto conseguira evitar la sobrecarga de información)
- Tener una base de datos de los clientes, que almacene: 
	- Nombre, Apellidos. 
	- Correo Electrónico: También podríamos enviar un correo electrónico cuando el usuario lleve mucho tiempo sin revisarse la vista.
	- DNI: Utilizaremos el DNI para ubicar al usuario y mostrarle  un mensaje de alerta cuando lleve mucho tiempo sin revisarse la vista.
